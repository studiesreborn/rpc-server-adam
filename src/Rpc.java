import org.apache.xmlrpc.WebServer;
import rpcModules.MathModule;
import rpcModules.PwrModule;
import rpcModules.RPCModule;
import rpcModules.TextModule;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Adam on 28.03.2016.
 */
public class RPC {

    private List<RPCModule> modules;
    private WebServer webServer;

    public RPC(List<RPCModule> modules) {
        this.modules = modules;
    }

    public String showServices(){
        return modules.stream()
                .map(RPCModule::methodsList)
                .collect(Collectors.joining("\n"));
    }

    public void run() {
        webServer = new WebServer(8023);
        modules.stream().forEach(rpcModule -> {
            webServer.addHandler(rpcModule.handlerName(), rpcModule);
        });
        webServer.addHandler("rpc",this);
        webServer.start();
    }


    public static void main(String[] args) {
        List<RPCModule> modules = new LinkedList<>();
        modules.add(new MathModule());
        modules.add(new TextModule());
        modules.add(new PwrModule());
        RPC rpc = new RPC(modules);
        System.out.println(rpc.showServices());
        rpc.run();
    }
}
