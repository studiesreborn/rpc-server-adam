package rpcModules;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Adam on 28.03.2016.
 */
public class PwrModule implements RPCModule {
    private ArrayList<WeekendWithClasses> classes;

    public PwrModule() {
        classes = new ArrayList<WeekendWithClasses>();
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yy");
            classes.add(new WeekendWithClasses(dateFormatter.parse("27.02.16"), dateFormatter.parse("28.02.16"), Week.odd));
            classes.add(new WeekendWithClasses(dateFormatter.parse("05.03.16"), dateFormatter.parse("06.03.16"), Week.even));
            classes.add(new WeekendWithClasses(dateFormatter.parse("19.03.16"), dateFormatter.parse("20.03.16"), Week.odd));
            classes.add(new WeekendWithClasses(dateFormatter.parse("02.04.16"), dateFormatter.parse("03.04.16"), Week.even));
            classes.add(new WeekendWithClasses(dateFormatter.parse("23.04.16"), dateFormatter.parse("24.04.16"), Week.odd));
            classes.add(new WeekendWithClasses(dateFormatter.parse("07.05.16"), dateFormatter.parse("08.05.16"), Week.even));
            classes.add(new WeekendWithClasses(dateFormatter.parse("21.05.16"), dateFormatter.parse("22.05.16"), Week.odd));
            classes.add(new WeekendWithClasses(dateFormatter.parse("28.05.16"), dateFormatter.parse("29.05.16"), Week.even));
            classes.add(new WeekendWithClasses(dateFormatter.parse("04.06.16"), dateFormatter.parse("05.06.16"), Week.odd));
        } catch (Exception ignored) {}
    }

    public String allClassWeekends() {
        return weekendClassesBetweenDates(classes.get(0).start,classes.get(classes.size()-1).end);
    }


    public String futureClassWeekends(){
        return weekendClassesBetweenDates(new Date(),classes.get(classes.size()-1).end);
    }

    public String weekendClassesBetweenDates(Date start, Date end){
        return classes.stream()
                .filter(weekendWithClasses -> this.isWeekendClassBetweenDates(weekendWithClasses,start,end))
                .map(WeekendWithClasses::description)
                .collect(Collectors.joining("\n"));
    }

    private boolean isWeekendClassBetweenDates(WeekendWithClasses weekendClass, Date start, Date end){
        return (weekendClass.start.after(start) || weekendClass.start.equals(start)) && (weekendClass.end.before(end) || weekendClass.end.equals(end));
    }

    private boolean isFutureWeekendClass(WeekendWithClasses weekend) {
        return weekend.end.after(new Date());
    }

    @Override
    public String methodsList() {
        return handlerName()+".allClassWeekends() -> String\n" +
                handlerName()+".futureClassWeekends() -> String\n"+
                handlerName()+".weekendClassesBetweenDates(Date, Date) -> String";
    }

    @Override
    public String handlerName() {
        return "pwr";
    }
}
