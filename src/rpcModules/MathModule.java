package rpcModules;

/**
 * Created by Adam on 28.03.2016.
 */
public class MathModule implements RPCModule {

    public Integer min(int x, int y){
        return Math.min(x,y);
    }

    @Override
    public String methodsList() {
        return handlerName()+".min(int, int) -> int";
    }

    @Override
    public String handlerName() {
        return "math";
    }
}
