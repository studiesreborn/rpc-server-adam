package rpcModules;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeekendWithClasses {
    public Date start;
    public Date end;
    public Week week;
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");

    public WeekendWithClasses(Date start, Date end, Week week) {
        this.start = start;
        this.end = end;
        this.week = week;
    }

    public String description() {
        String weekendTitle = week == Week.odd ? "Zjazd nieparzysty" : "Zjazd parzysty";

        return weekendTitle + ":\n" +
                dateFormatter.format(start) + "\n" +
                dateFormatter.format(end);
    }
}
