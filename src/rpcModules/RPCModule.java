package rpcModules;

/**
 * Created by Adam on 28.03.2016.
 */
public interface RPCModule {
    String methodsList();
    String handlerName();
}
