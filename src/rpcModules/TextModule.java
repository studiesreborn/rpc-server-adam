package rpcModules;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Created by Adam on 28.03.2016.
 */
public class TextModule implements RPCModule {

    public String encodeBase64(String text){
        String encoded = "";
        try {
            encoded = new String(Base64.getEncoder().encode(text.getBytes()),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encoded;
    }

    @Override
    public String methodsList() {
        return handlerName()+".encodeBase64(String) -> String";
    }

    @Override
    public String handlerName() {
        return "text";
    }
}
